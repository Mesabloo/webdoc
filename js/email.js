const generateEmailTemplate = (author, title, message) => {
    return `<div class="email_container"><span class="author">${author}</span><div class="title">${title}</div><hr><div class="content">${message}</div></div><hr>`
}

var showEmailClient = () => {
    $('#email_window').dialog('open')

    $('#email_window open').click((e) => {
        console.info($(this))
        switch (e.target.getAttribute('win')) {
            case 'terminal':
                showTerminal()
                break;
            case 'image':
                addImage(e.target.getAttribute('src'), e.target.getAttribute('title'))
                showImageViewer()
                break;
            case 'video':
                updateVideo(e.target.getAttribute('src'))
                showVideoPlayer()
                break;
            default:
                console.error(e.target)
        }
    })
}

var hideEmailClient = () => {
    $('#email_window').dialog('destroy')
    initEmailClient()
}

var initEmailClient = () => {
    $('#email_window').dialog({
        draggable: true,
        width: $('#bootloader').width() * (50/100),
        height: $('#bootloader').height() * (60/100),
        title: "Email client",
        hide: {
            effect: "explode",
            duration: 1000
        },
        resizable: false,
        autoOpen: false
    })
}

// init dialog box
initEmailClient()