var showTerminal = () => {
    $('#terminal_window .terminal .shell').html(`${localStorage.getItem("currently_logged_in")}@system: $`)

    $('#terminal_window').dialog('open')

    $('#terminal_window .terminal input').keypress((e) => {
        if (e.which == 13) {
            // we pressed enter, so let's execute the command
            e.preventDefault()
            let cmd = $('#terminal_window .terminal input').val()
            let level = localStorage.getItem(`level_${localStorage.getItem('currently_logged_in')}`)
            $('#terminal_window .output').html('')
            switch (cmd.split(' ')[0]) {
                case 'help':{
                    // depends on the level stored in the local storage
                    if (!level) {
                        level = ''
                        localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, '')
                    }
                    switch (level) {
                        case '':{
                            $('#terminal_window .output').html('<br>$attack<br>$practice<br>$cultivate<br>$hats<br>$fight<br>$hack<br>$report<br>$help<br>$menu')
                            break;
                        }case '0':{
                            $('#terminal_window .output').html('<br>$create &#60;virus><br>$report<br>help<br>$menu')
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>$report<br>$help<br>$menu')
                            break;
                        }case '1':{
                            $('#terminal_window .output').html('<br>$attack<br>$secure<br>$report<br>$help<br>$menu')
                            break;
                        }case '10':{
                            $('#terminal_window .output').html('<br>$ddos<br>$phish<br>$spam<br>$deploy &#60;malware><br>$report<br>$help<br>$menu')
                            break;
                        }case '2':{
                            $('#terminal_window .output').html('<br>$watch &#60;serie|film><br>$play &#60;game><br>$report<br>$help<br>$menu')
                            break;
                        }case '3':{
                            $('#terminal_window .output').html('<br>$buy &#60;grey|black|white><br>$report<br>$help<br>$menu')
                            break;
                        }case '5':{
                            $('#terminal_window .output').html('<br>$operate<br>$act<br>$meet &#60;person><br>$report<br>$help<br>$menu')
                            break;
                        }
                    }
                    break;
                }case 'menu':{
                    localStorage.setItem('level_'+localStorage.getItem('currently_logged_in'), '')
                    break;
                }case 'report':{
                    let email = generateEmailTemplate("x92", "Carnet de bord", "Voilà voilà, vous avez accès à notre carnet de bord. Cliquez <a href=\"https://docs.google.com/document/d/1nNF6pSqmOuucc0wqu81hZN9TrfkTxdQlllT6hRTQNuo/edit?usp=sharing\">ici</a>.")
                    $('#email_window').append(email)
                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                        showEmailClient()
                    }, false)
                    break;
                }case 'attack':{
                    switch (level) {
                        case '':{
                            localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, '0')
                            // send email
                            break;
                        }case '1':{
                            localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'0')
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'create':{
                    switch (level) {
                        case '0':{
                            switch (cmd.split(' ')[1]) {
                                case 'melissa':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'0')
                                    let email = generateEmailTemplate("x92", "Le virus « Melissa »", "L'arrivée de Windows 95 et d'Internet marque le début de l'âge d'or des virus. Les utilisateurs de Microsoft Office ont été assaillis de virus « macro », du nom du langage de programmation utilisé dans les documents Word, Excel et PowerPoint. Ils se propageaient de moins en moins par disquettes et de plus en plus par le réseau. L'apogée est atteinte avec Melissa en 1999, un virus macro qui se diffusait par email, chaque utilisateur impacté renvoyant le document infecté sans le savoir à une cinquantaine de ses contacts. Résultat: Melissa aurait infecté jusqu'à 20% des ordinateurs de la planète et généré 385 millions de dollars de dommage, en raison des surcharges. Son auteur a fait 20 mois de prison.<br>L'infection se répandait via les utilisateurs ayant Word (versions 97 ou 2000) ou ceux ayant Outlook (version 97 ou 98). Les autres versions (word 95 ou Outlook Express) n'étaient pas vulnérables à l'infection.<br>Cette infection avait deux conséquences :<br>&nbsp;&nbsp;&nbsp;&nbsp;- Saturation des systèmes de messagerie<br>&nbsp;&nbsp;&nbsp;&nbsp;- Tous les documents infectés d'une entreprise ou d'un particulier étaient envoyés à de multiples adresses, qu'ils soient confidentiels ou non.")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'i_love_you':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'1')
                                    let email = generateEmailTemplate("x92", "Le virus « I love you »", "A partir des années 2000, les virus informatiques au sens strict du terme ont perdu en importance. Les pirates ont concentré leur développement sur les vers qui, à l'inverse des virus, sont autonomes et n'ont pas besoin d'infecter un fichier ou un programme existant. N'ayant pas besoin d'un hôte, ils sont plus mobiles et plus néfastes. Ils rentrent sur un système en exploitant une vulnérabilité, se répliquent puis se propagent par le réseau. Apparu en 2000, le ver « I love you » remplaçait certains fichiers de l'ordinateur par lui-même, téléchargeait un cheval de Troie pour voler des mots de passe, puis se propageait par la messagerie. En l'espace de 10 jours, il a réussi à infecter plus de 50 millions de PC. Il aurait généré, au total, plus de 5 milliards de dollars de dommages.<br>Il cachait un script VBS malicieux derrière une fausse lettre d’amour. Ce script permettait la propagation de ce ver à travers une diffusion massive grâce à Outlook. Il ajoutait des clefs dans la base de registre lui permettant de se lancer à chaque démarrage de Windows.<br><br>Pour plus d'informations: <open win='video' src='9BtxDdq5dwc'>vidéo</open>")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'conficker':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'2')
                                    let email = generateEmailTemplate("x92", "Le virus « Conficker »", "Les années suivantes ont été un véritable festival de vers. Code Red a infecté les serveurs web Microsoft IIS en 2001. Blaster s'est introduit dans les systèmes Windows XP et Windows 2000 en 2003. Slammer et MyDoom prennent le relais un an plus tard. Le ver Conficker marque les esprits en 2008 en infectant presque 9 millions d'ordinateur selon l'éditeur F-Secure, ce qui en ferait l'une des plus grandes infections des années 2000. Mais surtout, Conficker a permis de créer l’un des plus grands botnets de l’histoire informatique. Cette nouvelle forme de malware s’est développée depuis le milieu des années 2000. Par la suite, les botnets deviendront de plus en plus sophistiqués. Leurs buts sont clairement crapuleux : vols d’informations, DDoS, minage de bitcoin, etc. Ils représentent, jusqu’à ce jour, une menace majeure que les forces de l’ordre ont bien du mal à combattre.<br>Ce ver exploite une faille du Windows Server Service utilisé par Windows 2000, Windows XP, Windows Vista, Windows 7, Windows Server 2003 et Windows Server 2008.<br>Quand il est installé dans un PC, Conficker met hors service certaines fonctions du système, telles que Windows Update, le centre de sécurité Windows, Windows Defender et Windows Error Reporting. Depuis sa version B, il tente également de se protéger lui-même, en empêchant par exemple l'accès à certains sites tels que Windows Update et de nombreux autres sites d'antivirus qui pourraient publier une mise à jour permettant de le détecter. Puis il tente de se connecter à de multiples serveurs pseudo-aléatoires, d'où il peut recevoir des ordres supplémentaires pour se propager, récolter des informations personnelles, télécharger et installer des logiciels malveillants additionnels sur les ordinateurs des victimes. Depuis sa version C, le virus a également un module peer-to-peer, tout comme Storm Worm l'avait déjà fait, lui permettant ainsi de se mettre à jour. Le ver s'attache aussi lui-même à certains fichiers de processus Windows critiques tel que: svchost.exe, explorer.exe et services.exe.<br>Le département de la Défense des États-Unis, le ministère de la Défense britannique et le ministère de la Défense français ont été attaqués par le ver. Au Royaume-Uni, il a infecté certains systèmes, dont le NavyStar/N* à bord de navires et sous-marins de la Royal Navy, et ceux de 800 ordinateurs d'hôpitaux de la région de Sheffield. En France, il a infecté Intramar, le réseau intranet de la Marine nationale, certains ordinateurs de la base aérienne 107 Villacoublay et de la direction interarmées des réseaux d'infrastructure et des systèmes d'information (DIRISI). Le virus aurait paralysé les 15-16 janvier 2009 les chasseurs Rafale de l'aviation navale, ce que dément le Ministère de la Défense français.<br><br>Pour plus d'informations : <open win='video' src='5FuQ9aRZL3E'>vidéo</open>")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'stuxnet':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'3')
                                    let email = generateEmailTemplate("x92", "Le virus « Stuxnet »", "En juin 2010, le grand public découvre l’existence du premier ver militaire : Stuxnet. Extrêmement sophistiqué, il a été créé par les services secrets américains et israéliens dans l’objectif de saboter en douce des installations nucléaires iraniennes. Pour cela, il était capable de se propager même sur des ordinateurs déconnectés de tout réseau, grâce à un mécanisme d’infection par clés USB. Mais ses auteurs, voulant aller trop vite, ont rendu le ver trop agressif et celui-ci s’est propagé bien au-delà du territoire iranien, alertant nombre de chercheurs en sécurité. En février 2016, un documentaire montre que Stuxnet n’était qu’une partie d’une opération de cyber sabotage à l’échelle nationale, baptisée « Nitro Zeus ». Toutes les infrastructures critiques de l’Iran auraient ainsi été infectées par des « implants » capable de les « perturber, dégrader ou détruire ». Bref, le malware se met à l’heure de la guerre cybernétique.<br><br>Pour plus d'informations : <open win='video' src='7g0pi4J8auQ'>vidéo</open>")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'mirai':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'4')
                                    let email = generateEmailTemplate("x92", "Le virus « Mirai »", "Créé par un pirate qui se fait appeler « Anna-Senpai », ce botnet est l'un des phénomènes cybercriminels qui ont le plus marqué l'année 2016. Principalement constitué d'objets connectés, ce réseau d'appareils zombies a été à l'origine de plusieurs attaques spectaculaires par déni de service distribué (DDoS). Début octobre, il arrive à déconnecter le site du journaliste Brian Krebs et à inonder les serveurs de l'hébergeur OVH. Quelques semaines plus tard, il parvient à mettre à genoux une partie du web américain, en s'attaquant à un important prestataire Internet (Dyn). En faisant cela, Mirai révèle non seulement la piètre sécurité des objets connectés, mais aussi les vulnérabilités de l’écosystème de l’Internet. Autre surprise : l’enquête réalisée par Brian Krebs montre qu’Anna-Senpai est en réalité un fournisseur de services anti-DDoS qui propose à ses clients l’antidote du poison qu’il crée lui-même. Effrayant.<br><br>Pour plus d'informations : <open win='video' src='qgazLNZ0itE'>vidéo</open>")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'wannacry':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'5')
                                    let email = generateEmailTemplate("x92", "le virus « WannaCry »", "Le 12 mai 2017, le mot « Ransomware » n’est plus un secret pour personne. En quelques jours, un ver baptisé WannaCry chiffre les disques de centaines de milliers d’ordinateurs Windows dans les entreprises, perturbant ainsi pêle-mêle les services dans les hôpitaux, les gares, les centres commerciaux, etc. Même la production industrielle de Renault a été impactée. Le comble, c’est que WannaCry est indirectement l’œuvre de la NSA. En effet, l’agence de renseignement américaine s’est fait voler une partie de ses outils de piratage. Ils ont été diffusés par petits bouts par le mystérieux groupe de pirates Shadow Brokers, qui est probablement le faux nez d’une organisation gouvernementale. Quant à WannaCry, il semblerait que le régime de Pyongyang n’y soit pas totalement étranger. Bienvenu dans le monde des coups tordus et de l’intox politico-diplomatique. Les attaques informatiques, on le voit, servent désormais à tout.<br>En mai 2017, il est utilisé lors d'une cyberattaque mondiale massive, touchant plus de 300 000 ordinateurs, dans plus de 150 pays, principalement en Inde, aux États-Unis et en Russie et utilisant le système obsolète Windows XP et plus généralement toutes les versions antérieures à Windows 10 n'ayant pas effectué les mises à jour de sécurité, en particulier celle du 14 mars 2017.<br>Cette cyberattaque est considérée comme le plus grand piratage à rançon de l'histoire d'Internet, l'office européen des polices Europol la qualifiant « d'un niveau sans précédent » et ajoutant « qu'il ne faut en aucun cas payer la rançon ».<br>Parmi les plus importantes organisations touchées par cette attaque, on retrouve notamment les entreprises Vodafone, FedEx, Renault, Telefónica, le National Health Service, le ministère de l'Intérieur russe ou encore la Deutsche Bahn.<br>Ce virus a refait parler de lui en s'attaquant le 23 juin 2017 à une des usines du groupe automobile Honda, située à Sayama, au Japon, et cela intervient cinq jours avant le début d'une autre grande cyberattaque mondiale, NotPetya.<br><br>Pour plus d'informations : <open win='video' src='etPizFNPupk'>vidéo</open>")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }default:{
                                    $('#terminal_window .output').html('<br>Unknown virus \''+cmd.split(' ')[1]+'\'. Available ones are: melissa, i_love_you, conficker, stuxnet, mirai, wannacry')
                                    break;
                                }
                            }
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'secure':{
                    switch (level) {
                        case '1':{
                            localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'1')
                            let email = generateEmailTemplate("x92", "La cybersécurité", "Réalisations de tests d’intrusion, de DDoS (attaque par déni de service), utilisation d’ingénierie sociale (manipulation psychologique) pour tester le comportement des utilisateurs, de scanners de vulnérabilités, réalisation de frameworks comme Metasploit (Son but est de fournir des informations sur les vulnérabilités de systèmes informatiques)")
                            $('#email_window').append(email)
                            localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                            showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                showEmailClient()
                            }, false)
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'practice':{
                    switch (level) {
                        case '':{
                            localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, '1')
                            $('#terminal_window .output').html('<br>Let\'s just choose between security and malwares.')
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'cultivate':{
                    switch (level) {
                        case '':{
                            localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, '2')
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'hats':{
                    switch (level) {
                        case '':{
                            localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, '3')
                            $('#terminal_window .output').html('<br>Now just by a hat with `$buy <grey|black|white>`')
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'fight':{
                    switch (level) {
                        case '':{
                            localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, '4')
                            let email = generateEmailTemplate("x92", "La lutte contre le hacking", "Prévention, communiquer sur le sujet : Beaucoup  de personne <br>S’équiper contre les menaces (antivirus) : La meilleure protection, c’est vous ! On a tendance à dire que les antivirus sont la meilleur solution pour se protéger des virus mais en réalité les protections déjà fournis par votre système d’exploitation sont amplements suffisantes. Il suffit d’adopter une attitude sur Internet pour être au mieux protéger contre les virus et surtout être informé sur les menaces présentes en ligne.<br>Mettre ses logiciels à jour : Vos systèmes d’exploitations et vos outils connectés à Internet (comme les navigateurs, boites mail) doivent être mis à jour régulièrement. De manière générale, vous êtes notifié lorsqu’une mise à jour de vos programmes est disponible")
                            $('#email_window').append(email)
                            localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                            showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                showEmailClient()
                            }, false)
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'hack':{
                    switch (level) {
                        case '':{
                            localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, '5')
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'ddos':{
                    switch (level) {
                        case '10':{
                            localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'0')
                            $('#terminal_output .output').html('<br>Failed to send email')
                            setTimeout(() => {
                                let email = generateEmailTemplate("x92", "L'attaque DDoS", "DDoS attack (attaque par déni de service) : Une attaque par déni de service (DoS) est une attaque informatique ayant pour but de rendre indisponible un service, d'empêcher les utilisateurs légitimes d'un service de l'utiliser. À l’heure actuelle la grande majorité de ces attaques se font à partir de plusieurs sources, on parle alors d'attaque par déni de service distribuée (DDoS).")
                                $('#email_window').append(email)
                                localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                    showEmailClient()
                                }, false)
                            }, 7500)
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'phish':{
                    switch (level) {
                        case '10':{
                            localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'1')
                            let email = generateEmailTemplate("x92", "Le phising", "Phishing (Hameçonnage) : Le phishing est une technique utilisée par des fraudeurs pour obtenir des renseignements personnels dans le but de perpétrer une usurpation d'identité. La technique consiste à faire croire à la victime qu'elle s'adresse à un tiers de confiance : banque, administration, etc,  afin de lui soutirer des renseignements personnels : mot de passe, numéro de carte de crédit, numéro ou photocopie de la carte nationale d'identité, date de naissance, etc. En effet, le plus souvent, une copie exacte d'un site internet est réalisée dans l'optique de faire croire à la victime qu'elle se trouve sur le site internet officiel où elle pensait se connecter. La victime va ainsi saisir ses codes personnels qui seront récupérés par celui qui a créé le faux site, il aura ainsi accès aux données personnelles de la victime et pourra dérober tout ce que la victime possède sur ledit site web. L’attaque peut aussi être réalisée par courrier électronique ou autres moyens électroniques.")
                            $('#email_window').append(email)
                            localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                            showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                showEmailClient()
                            }, false)
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'spam':{
                    switch (level) {
                        case '10':{
                            localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'2')
                            let email = generateEmailTemplate("x92", "Les spams", "Spam (Pourriel) : Le spam, courriel indésirable ou pourriel est une communication électronique non sollicitée, en premier lieu via le courrier électronique. Il s'agit en général d'envois en grande quantité effectués à des fins publicitaires.")
                            $('#email_window').append(email)
                            localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                            showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                showEmailClient()
                            }, false)
                            let email1 = generateEmailTemplate("x92", "Les spams", "Spam (Pourriel) : Le spam, courriel indésirable ou pourriel est une communication électronique non sollicitée, en premier lieu via le courrier électronique. Il s'agit en général d'envois en grande quantité effectués à des fins publicitaires.")
                            $('#email_window').append(email1)
                            localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email1)
                            showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                showEmailClient()
                            }, false)
                            let email2 = generateEmailTemplate("x92", "Les spams", "Spam (Pourriel) : Le spam, courriel indésirable ou pourriel est une communication électronique non sollicitée, en premier lieu via le courrier électronique. Il s'agit en général d'envois en grande quantité effectués à des fins publicitaires.")
                            $('#email_window').append(email2)
                            localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email2)
                            showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                showEmailClient()
                            }, false)
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'deploy':{
                    switch (level) {
                        case '10':{
                            switch (cmd.split(' ')[1]) {
                                case 'virus':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'0')
                                    let email = generateEmailTemplate("x92", "Les virus", "Virus (computer virus) : Un virus informatique est un automate autoréplicatif à la base non malveillant, mais aujourd'hui souvent additionné de code malveillant, conçu pour se propager à d'autres ordinateurs en s'insérant dans des logiciels légitimes, appelés « hôtes ». Il peut perturber plus ou moins gravement le fonctionnement de l'ordinateur infecté. Il peut se répandre par tout moyen d'échange de données numériques comme les réseaux informatiques et les cédéroms, les clefs USB, les disques durs, etc.")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'worm':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'1')
                                    let email = generateEmailTemplate("x92", "Les vers informatique", "Ver (computer worm) : Un ver informatique est un logiciel malveillant qui se reproduit sur plusieurs ordinateurs en utilisant un réseau informatique comme Internet. Il a la capacité de se dupliquer une fois qu'il a été exécuté. Contrairement au virus, le ver se propage sans avoir besoin de se lier à d'autres programmes exécutables.")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'trojan':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'2')
                                    let email = generateEmailTemplate("x92", "Les troyens", "Cheval de Troie (trojan horse) : Un cheval de Troie est un type de logiciel malveillant, qui ne doit pas être confondu avec les virus ou autres parasites. Le cheval de Troie est un logiciel en apparence légitime, mais qui contient une fonctionnalité malveillante. Le rôle du cheval de Troie est de faire entrer ce parasite sur l'ordinateur et de l'y installer à l'insu de l'utilisateur.")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'spyware':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'3')
                                    let email = generateEmailTemplate("x92", "Les spywares", "Spyware (logiciel espion) : Un logiciel espion est un logiciel malveillant qui s'installe dans un ordinateur ou autre appareil mobile, dans le but de collecter et transférer des informations sur l'environnement dans lequel il s'est installé, très souvent sans que l'utilisateur en ait connaissance. L'essor de ce type de logiciel est associé à celui d'Internet qui lui sert de moyen de transmission de données.")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'bot':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'4')
                                    let email = generateEmailTemplate("x92", "Les botnets", "Bot : Un bot informatique est un agent logiciel automatique ou semi-automatique qui interagit avec des serveurs informatiques. Un bot se connecte et interagit avec le serveur comme un programme client utilisé par un humain, d'où le terme « bot », qui est la contraction par aphérèse de « robot ». Il a plusieurs utilisations possibles : Botnet (réseau de bots informatiques qui communiquent avec d'autres programmes similaires pour l'exécution de certaines tâches avec création de machines zombies (ordinateur contrôlé à l'insu de son utilisateur par un cybercriminel), spambot (utiliser pour spammer) et webbots (fraude au clic : activité qui consiste à faire effectuer, par une personne ou un programme informatique, des clics sur des publicités afin de dilapider rapidement le budget publicitaire d’un concurrent en lui faisant payer des amendes à son insu).")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'ransomware':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'5')
                                    let email = generateEmailTemplate("x92", "Les rançongiciels", "Ransomware (Rançongiciel) : Un ransomware est un logiciel malveillant qui prend en otage des données personnelles. Pour ce faire, il chiffre des données personnelles puis demande à leur propriétaire d'envoyer de l'argent en échange de la clé qui permettra de les déchiffrer.")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'scareware':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'6')
                                    let email = generateEmailTemplate("x92", "Les scarewares", "Scareware : Un scareware est une forme de logiciel malveillant qui utilise l'ingénierie sociale pour provoquer le choc, l'anxiété ou la perception d'une menace afin de manipuler les utilisateurs à acheter des logiciels indésirables. Le scareware fait partie d'une classe de logiciels malveillants qui inclut les faux logiciel de sécurité, les logiciels de rançon et d'autres logiciels d'escroquerie qui suggère de payer pour télécharger de faux logiciels pour les supprimer. Habituellement, le logiciel est fictif, non-fonctionnel ou directement malveillant.")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }default:{
                                    $('#terminal_window .output').html('<br>Unknown malware \''+cmd.split(' ')[1]+'\'. Available ones are: virus, worm, trojan, spyware, bot, ransomware, scareware')
                                    break;
                                }
                            }
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'watch':{
                    switch (level) {
                        case '2':{
                            switch (cmd.split(' ')[1]) {
                                case 'operation_espadon':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'0')
                                    let email = generateEmailTemplate("x92", "Le film « Opératon Espadon »", "Opération Espadon ou Opération Swordfish au Québec (Swordfish) est un thriller américain de Dominic Sena sorti en 2001.<br>Stanley, un prodigieux hacker, est engagé par le mystérieux Gabriel et son assistante Ginger, afin de créer un virus informatique. Le but est de récupérer l'argent trouble oublié sur des comptes de l'État. En échange, Gabriel s'engage à aider financièrement Stanley pour qu'il obtienne juridiquement la garde de sa fille.<br><br>Pour plus d'informations : <open win='video' src='LN2V1EfWkBw'>vidéo</open>")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'hackers':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'1')
                                    let email = generateEmailTemplate("x92", "Le film « Hackers »", "Hackers est un film américain réalisé par Iain Softley, sorti en 1995. Connu aussi sous les titres francophones de Hackers : Les Pirates du cyberespace (titre DVD et Universal Media Disc) et Pirates (titre télévisé).<br>Ils peuvent forcer n'importe quel code et pénétrer dans n'importe quel système. Ils sont souvent encore adolescents et déjà mis sous surveillance par les autorités. Ce sont des hackers. « Zero Cool » - de son vrai nom Dade Murphy - est une légende parmi ses pairs. En 1988, il a, à lui tout seul, cassé mille cinq cents sept ordinateurs de Wall Street et la justice lui a interdit de s'approcher d'un clavier avant d'avoir 18 ans. Il est resté sept ans sans ordinateur. Kate Libby, autrement dit « Acid Burn », a un ordinateur portable gonflé, qui peut passer de zéro à soixante sur l'autoroute de l'information en une nanoseconde. Lorsqu'ils entrent en collision, la guerre des sexes prend un tour particulièrement grave. Mais il devient impossible de dire ce qui va se passer lorsque le maître hacker « La Peste » utilise Dade, Kate et leurs amis dans une diabolique conspiration industrielle. Maintenant eux seuls peuvent éviter une catastrophe comme le monde n'en a jamais vu.<br><br>Pour plus d'informations : <open win='video' src='c32Vt8IDf5s'>vidéo</open>")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'hacker':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'2')
                                    let email = generateEmailTemplate("x92", "Le film « Hacker »", "Hacker (Blackhat) est un techno thriller américain coproduit, coécrit et réalisé par Michael Mann, sorti en 2015.<br>Le hacking de la centrale nucléaire de Chai Wan à Hong Kong provoque une importante explosion. Le gouvernement chinois dépêche l'un de ses meilleurs experts informatiques, le capitaine Chen Dawai. Ce dernier doit également collaborer avec le FBI car le Chicago Mercantile Exchange a été attaqué avec le même RAT (« Remote Access Tool »). Chen reconnaît dans ce code la « signature » d'un ancien camarade d'école, Nicholas Hathaway, un pirate informatique qui purge actuellement une peine de prison… Ce dernier est donc libéré s'il accepte de collaborer avec le FBI et le gouvernement chinois pour démasquer le coupable de cette attaque informatique.<br><br>Pour plus d'informations : <open win='video' src='gvguTQ9uPSA'>vidéo</open>")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'mr_robot':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'3')
                                    let email = generateEmailTemplate("x92", "La série « Mr Robot »", "Mr. Robot est une série télévisée américaine créée par Sam Esmail et diffusée depuis le 24 juin 2015 sur USA Network et depuis le 5 septembre 2015 sur Showcase au Canada.<br>Elliot Alderson est un jeune informaticien vivant à New York, qui travaille en tant que technicien en sécurité informatique pour Allsafe Security. Celui-ci luttant constamment contre un Trouble dissociatif de l'identité et de dépression, son processus de pensée semble fortement influencé par la paranoïa et l'illusion. Il pirate les comptes des gens, ce qui le conduit souvent à agir comme un cyber-justicier. Elliot rencontre « Mr. Robot », un mystérieux anarchiste qui souhaite le recruter dans son groupe de hackers connu sous le nom de « Fsociety ». Leur objectif consiste à rétablir l'équilibre de la société par la destruction des infrastructures des plus grosses banques et entreprises du monde, notamment le conglomérat E Corp. (surnommé « Evil Corp. » par Elliot) qui, par ailleurs, représente 80 % du chiffre d’affaires de Allsafe Security.<br><br>Pour plus d'informations : <open win='video' src='uml6bz9ygxA'>vidéo</open>")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }default:{
                                    $('#terminal_window .output').html('<br>Unknown movie/serie \''+cmd.split(' ')[1]+'\'. Available ones are: operation_espadon, hackers, hacker, mr_robot')
                                    break;
                                }
                            }
                            
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'play':{
                    switch (level) {
                        case '2':{
                            switch (cmd.split(' ')[1]) {
                                case 'hacknet':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'0')
                                    let email = generateEmailTemplate("x92", "Le jeu « Hacknet »", "Hacknet est un jeu vidéo de simulation développé par Team Fractal Alligator et édité par Surprise Attack Games, sorti en 2015 sur Windows, Mac et Linux.<br>On se retrouve donc dans la peau d’un jeune hacker débutant qui doit faire ses preuves. On est rapidement approché par des groupes de hacker qui ont vite remarqué notre petit talent sortant du lot. On se retrouve donc « embauché » par ce groupe qui nous propose des contrats, jusqu’au jour où l’un des membres les plus importants (le meilleur hacker) disparaît sans laisser de trace ni donner la moindre nouvelle...<br>L’histoire peut sembler faible, voire inexistante, mais la « force » de ce jeu, c’est que la fable est dissimulée tout au long de l’aventure et que l’on peut même passer complètement à côté si l’on ne prend pas la peine de bien tout « explorer » durant nos séances de piratage.<br>En effet,le jeu regorge de petites histoires « secondaires », parfois drôles, parfois tristes, que l’on peut trouver de-ci de-là dans les ordinateurs ou sur les serveurs que l’on pirate, ce qui ajoute du « réalisme».<br><br>Pour plus d'informations : <open win='video' src='zrDRMXRxmxY'>vidéo</open>")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'watch_dogs':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'1')
                                    let email = generateEmailTemplate("x92", "Le jeu « Watch Dogs »", "Watch Dogs (stylisé WATCH_DOGS, trad. litt. : « Les chiens de garde ») est un jeu vidéo d'action-aventure et d'infiltration développé par les studios Ubisoft Montréal et Ubisoft Reflections. Il est annoncé par Ubisoft lors de sa conférence à l'E3 2012. Le jeu est sorti le 27 mai 2014 sur PC, Xbox 360, PlayStation 3, PlayStation 4 et Xbox One et en novembre 2014 sur Wii U.<br>Le titre Watch Dogs fait référence au personnage du jeu qui se présente comme le gardien de Chicago et qui n'hésite pas à retourner les infrastructures informatiques contre ses ennemis, en effet un watchdog est une procédure s'exécutant généralement à intervalles réguliers afin de vérifier certains points cruciaux garantissant un minimum d'intégrité au système ou au programme. En cas de situation anormale détectée, le watchdog peut simplement déclencher une alarme (exemple : envoyer un e-mail à quelqu'un) ou plus souvent lancer une procédure radicale supposée résoudre le problème à coup sûr (exemple : reboot du système ou redémarrage d'un service interrompu).<br>Watch Dogs a battu le record du jeu le plus vendu de l'éditeur Ubisoft le premier jour de commercialisation.<br>En une semaine de commercialisation, le jeu s'écoule à plus de 4 millions d'exemplaires dans le monde, devenant la nouvelle licence la plus rapide à atteindre ce chiffre.<br><br>Pour plus d'informations : <open win='video' src='e_q-s3QdmU8'>vidéo</open>")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'sorry_james':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'2')
                                    let email = generateEmailTemplate("x92", "Le jeu « Sorry, James!", "James est un hacker, la quarantaine, travaillant pour une grosse société d'armements, Mantis Corp.. Son travail consiste à décrypter des fichiers de l'entreprise, une tâche rébarbative qu'il assure coincé derrière son ordinateur à l'interface vieillotte. Et c'est sans plus de transition que le jeu nous téléporte sur cette station de travail, derrière un écran légèrement flou, à devoir entrer un login et un mot de passe. Après quelques essais infructueux, je me rappelle l'étrange description du titre sur Steam... Mais bien sûr !<br><br>Pour plus d'informations : <open win='video' src='eHqmF0Sa_34'>vidéo</open>")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }default:{
                                    $('#terminal_window .output').html('<br>Unknown game \''+cmd.split(' ')[1]+'\'. Available ones are: hacknet, watch_dogs, sorry_james')
                                    break;
                                }
                            }
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'buy':{
                    switch (level) {
                        case '3':{
                            switch (cmd.split(' ')[1]) {
                                case 'grey':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'0')
                                    let email = generateEmailTemplate("x92", "Le chapeau gris (Grey Hat)", "C’est un mélange de White Hat et de Black Hat .<br>Ce hacker agit des fois pour la bonne cause, comme un White Hat le fait mais peut commettre de temps à autre des délits.<br>Il s’introduit par exemple illégalement dans un système afin d’en prévenir ensuite les responsables des failles qu’il aura trouvées. Son action est louable, mais tout de même illégale.")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'black':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'1')
                                    let email = generateEmailTemplate("x92", "Le chapeau noir (Black Hat)", "Le hacker au chapeau noir peut être aussi expérimenté que celui au chapeau blanc, voire plus. Mais il agit par contre à des fins qui lui sont propres, et qui sont illégales.<br>Il vole des données, s’introduit illégalement dans les systèmes ou encore pirate des comptes.<br>C’est là qu’intervient le mot “pirate”. Le hacker au chapeau noir ne devrait pas être appelé “hacker”. Il s’agit malheureusement de la définition qu’on trouve souvent dans les médias : le terme hacker est utilisé à tort pour parler du pirate.")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'white':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'2')
                                    let email = generateEmailTemplate("x92", "Le chapeau blanc (White Hat)", "Il s’agit souvent d’une personne qui a atteint une maturité d’esprit ainsi que des qualifications suffisantes et approuvées par les autres.<br>Il aide les victimes, il aide à sécuriser les systèmes et combat contre la cybercriminalité.<br>Il travaille typiquement dans une grande entreprise pour sécuriser les systèmes et réseaux, mais il peut très bien être un jeune passionné ayant appris sur le tas.")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }default:{
                                    $('#terminal_window .output').html('<br>Unknown hat color \''+cmd.split(' ')[1]+'\'. Available ones are: grey, black, white')
                                    break;
                                }
                            }
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'operate':{
                    switch (level) {
                        case '5':{
                            localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'0')
                            let email = generateEmailTemplate("x92", "Des organisations connues", "Anonymous : Le groupe le plus connu et prolifique qui ont menés plusieurs attaques notamment contre l’église de scientologie aux Etats-Unis et même contre Daech après les attaques du 13 novembre.<br>WikiLeaks : Le but de l’organisation est de publier des documents confidentiels afin de lutter contre la corruption qui sévit dans notre société. Plusieurs millions de documents ont été postés sur leur site depuis sa création relatif à des scandales de corruption, d’espionnage, et de violations des droits de l’homme.")
                            $('#email_window').append(email)
                            localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                            showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                showEmailClient()
                            }, false)
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'act':{
                    switch (level) {
                        case '5':{
                            localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'1')
                            // send email
                            let email = generateEmailTemplate("x92", "Les différents domaines d'action", "Différents domaines d’actions : Géopolitique, religieux, censure, diplomatique")
                            $('#email_window').append(email)
                            localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                            showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                showEmailClient()
                            }, false)
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }case 'meet':{
                    switch (level) {
                        case '5':{
                            switch (cmd.split(' ')[1]) {
                                case 'snowden':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'0')
                                    let email = generateEmailTemplate("x92", "Edward Snowden", "Lanceur d’alerte Américain qui a révélé plusieurs scandales au sujet de la CIA et de la NSA.")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'swartz':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'1')
                                    let email = generateEmailTemplate("x92", "Aaron Swartz", "Militant politique et hacktiviste qui consacra sa vie à la liberté du numérique.")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }case 'assange':{
                                    localStorage.setItem(`level_${localStorage.getItem('currently_logged_in')}`, level+'2')
                                    let email = generateEmailTemplate("x92", "Julian Assange", "Cybermilitant australien connu pour être le fondateur de WikiLeaks")
                                    $('#email_window').append(email)
                                    localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in'))+'\n'+email)
                                    showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                                        showEmailClient()
                                    }, false)
                                    break;
                                }default:{
                                    $('#terminal_window .output').html('<br>Unknown person \''+cmd.split(' ')[1]+'\'. Available ones are: snowden, swartz, assange')
                                    break;
                                }
                            }
                            // send email
                            break;
                        }default:{
                            $('#terminal_window .output').html('<br>Unknown command \''+cmd.split(' ')[0]+'\'')
                            break;
                        }
                    }
                    break;
                }
            }

            $('#terminal_window .terminal input').val('')
        }
    })
}

var hideTerminal = () => {
    $('#terminal_window').dialog('destroy')
    initTerminal()
}

var initTerminal = () => {
    $('#terminal_window').dialog({
        draggable: true,
        width: $('#bootloader').width() * (50/100),
        height: $('#bootloader').height() * (45/100),
        title: "Terminal",
        hide: {
            effect: "explode",
            duration: 1000
        },
        resizable: false,
        autoOpen: false
    })
}

// init dialog box
initTerminal()