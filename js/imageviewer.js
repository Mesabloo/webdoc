const generateImageTemplate = (src, title) => {
    return `<div class="mySlides fade">
                <img src="${src}" style="width:100%">
                <div class="text">${title}</div>
            </div>`
}

var addImage = (src, title) => {
    let template = generateEmailTemplate(src, title)

    $('#imageviewer_window .slideshow-container').prepend(template)
}

// Next/previous controls
function plusSlides(n) {
    showSlides(slideIndex += n);
}
  
  // Thumbnail image controls
function currentSlide(n) {
    showSlides(slideIndex = n);
}
  
function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) {slideIndex = 1} 
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none"; 
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block"; 
    dots[slideIndex-1].className += " active";
}

var showImageViewer = () => {
    $('#imageviewer_window').dialog('open')
}

var hideImageViewer = () => {
    $('#imageviewer_window').dialog('destroy')
    initImageViewer()
}

var initImageViewer = () => {
    $('#imageviewer_window').dialog({
        draggable: true,
        width: $('#bootloader').width() * (35/100),
        height: $('#bootloader').height() * (50/100),
        title: "Image Viewer",
        hide: {
            effect: "explode",
            duration: 1000
        },
        resizable: false,
        autoOpen: false
    })
}

// init dialog box
initImageViewer()