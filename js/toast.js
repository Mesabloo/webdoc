var showToast = (title, message, callback, doesHide) => {
    $('#toast .title').html(title)
    $('#toast .message').html(message)

    $('#toast').click(() => {
        $('#toast').css('left', '-100%')

        callback()
    })

    $('#toast').css('left', '0px')

    if (doesHide)
        setInterval(() => {
            $('#toast').css('left', '-100%')
        })
}