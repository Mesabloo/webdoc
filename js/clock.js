var loadClockFeature = () => {
    setInterval(
        () => {
            let date = new Date()
            let hours = date.getHours()
            let mins = date.getMinutes()
            let h = (hours < 10?'0':'')+hours
            let m = (mins < 10?'0':'')+mins

            document.getElementById('clock').innerText = h+":"+m;
        }, 
        25 
    )
}

loadClockFeature()