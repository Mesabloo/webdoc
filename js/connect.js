let newUser = false;

let tryConnect = (e) => {
    if (e.preventDefault)
        e.preventDefault();
    let username = $('#login #box #username').val()
    let password = $('#login #box #password').val()
    console.info(`Username: ${username} / Password: ${password} / Hashed: ${md5(password)}`)
    let md5DBPass = localStorage.getItem(username)
    if (md5DBPass) {
        if (md5(password) === md5DBPass) {
            // successfully logged in
            console.log("successfully logged in")
            localStorage.setItem("currently_logged_in", username.toLowerCase())
            showDesktop()
        } else {
            // incorrect password, show it up
            console.error("error in password")
            $('#login #box #cannot_login').css('display', 'block')
            setTimeout(() => {
                $('#login #box #cannot_login').css('display', 'none')
            }, 4000)
        }
    } else {
        localStorage.setItem(username, md5(password))
        localStorage.setItem("currently_logged_in", username.toLowerCase())
        // launch introduction
        console.log("successfully logged in")
        newUser = true;
        showDesktop()
    }
}

$('#login #box .connect').click(tryConnect)

$('#login #box form').submit(tryConnect)

let showDesktop = () => {
    if (newUser) {
        $('#login #intro').css('display', 'inline-block')
        var audio = new Audio('assets/musics/Intro_WebDoc.ogg')
        audio.volume = 1
        audio.play()

        audio.addEventListener('ended', () => {
            $('#login #intro').css('display', 'none')

            let email = generateEmailTemplate("x92", "Hello and welcome into the Hack Operating System (Hack OS for short).<br>Please open a <open win=\"terminal\">terminal</open>, and type `help` (then Enter) to see all available commands at any time.<br>Hope you'll find the good path,<br>&nbsp;&nbsp;&nbsp;&nbsp;x92")
            $('#email_window').append(email)
            localStorage.setItem('emails_'+localStorage.getItem('currently_logged_in'), email)
            localStorage.setItem('level_'+localStorage.getItem('currently_logged_in'), '')
            showToast("New email", "You have a new email in your inbox.\nClick here to see it", () => {
                showEmailClient()
            }, false)

            background = new Audio('assets/musics/musique_fond.ogg')
            if ('loop' in background) {
                background.loop = true;
            } else {
                background.addEventListener('ended', () => {
                    this.currentTime = 0;
                    this.play();
                }, false);
            }
            background.volume = 0.5
            background.play()

            $('#login').css({
                'animation-name': 'fadeOutDown',
                'animation-duration': '0.5s',
                'animation-fill-mode': 'forwards'
            })
            $('#login #box #password').val('')
            $('#desktop').css('display', 'block')
        })
    } else {
        for (mail of localStorage.getItem('emails_'+localStorage.getItem('currently_logged_in')).split('\n')) {
            $('#email_window').append(mail)
            console.log(`mail loaded: ${mail}`)
        }
        console.info('Restoring last level...')

        background = new Audio('assets/musics/musique_fond.ogg')
        if ('loop' in background) {
            background.loop = true;
        } else {
            background.addEventListener('ended', () => {
                this.currentTime = 0;
                this.play();
            }, false);
        }
        background.volume = 0.5
        background.play()

        $('#login').css({
            'animation-name': 'fadeOutDown',
            'animation-duration': '0.5s',
            'animation-fill-mode': 'forwards'
        })
        $('#login #box #password').val('')
        $('#desktop').css('display', 'block')
    }
}

var background = new Audio('assets/musics/musique_fond.ogg')