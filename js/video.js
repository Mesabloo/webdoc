var showVideoPlayer = () => {
    $('#videoplayer_window').dialog('open')
}

var updateVideo = (videoLink) => {
    $('#videoplayer_window #video').attr('src', `https://www.youtube.com/embed/${videoLink}`)
}

var hideVideoPlayer = () => {
    $('#videoplayer_window').dialog('destroy')
    initVideoPlayer()
}

var initVideoPlayer = () => {
    $('#videoplayer_window').dialog({
        draggable: true,
        width: $('#bootloader').width() * (35/100),
        height: $('#bootloader').height() * (50/100),
        title: "Video Player",
        hide: {
            effect: "explode",
            duration: 1000
        },
        resizable: false,
        autoOpen: false
    })
}

// init dialog box
initVideoPlayer()