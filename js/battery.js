var loadBatteryFeature = () => {
    navigator.getBattery().then((battery) => {
        let b = Math.round(battery.level*100);

        document.getElementById('battery').innerText = (b < 10?'00':'')+b+'%';
    
        battery.onlevelchange = () => {
            b = Math.round(battery.level*100);
            document.getElementById('battery').innerText = (b < 10?'00':'')+b+'%';
        }
    }).catch((e) => {
        console.error('An error occured while updating the battery level: '+e)
    })
}

loadBatteryFeature()