var bootloader = () => {
    var sd
    let store = window.localStorage

    async function launch() {
        $('#taskbar, #taskbar > *').css('display', 'none')
        let width = $('#bootloader').css('width'),
            height = $('#bootloader').css('height')
        width = width.substr(0, width.length-2)
        height = height.substr(0, height.length-2)
        console.log(`Aspect ratio: ${width/height} / W: ${width} / H: ${height}`)
        if (width/height <= 1.5) {
            $('.start_unavailable').css('display', 'block')
            $('#bootloader').css('display', 'none')
        } else {
            $('#no_start').remove()
            $('html, body, #bootloader').css('cursor', 'none')

            await modifyHTML(null, msg1, [500, 250])
        }
    }
    
    async function modifyHTML(addText, func, timeout) {
        if (addText) {
            $('#bootloader').append(addText.replace(/\s\s/g, '&#160;&#160;'))
            $('#bootloader').scrollTop($('#bootloader').prop('scrollHeight'))
        }
    
        setTimeout(func, Math.random()*timeout[0]+timeout[1])
    }

    let msg1 = () => modifyHTML('<div>:: running early hook [udev]</div>', msg2, [150, 95])
    let msg2 = () => modifyHTML('<div>:: running early hook [lvm2]</div>', msg3, [175, 110])
    let msg3 = () => modifyHTML('<div>:: running hook [udev]</div>', msg4, [215, 140])
    let msg4 = () => modifyHTML('<div>:: Triggering uevents...</div>', msg5, [750, 500])
    let msg5 = () => modifyHTML('<div>:: running hook [keymap]</div>', msg6, [90, 80])
    let msg6 = () => modifyHTML('<div>:: Loading keymap...done.</div>', msg7, [120, 75])
    let msg7 = () => {
        let genSd = () => {
            let rand = Math.round(Math.random()*4)
            return (rand == 0?'a':rand == 1?'b':rand == 2?'c':rand == 3?'d':'e')
        }
        let genSdNo = () => {
            let rand = Math.random()*6
            return Math.trunc(rand)+1
        }
        if (!store.getItem('device')) {
            sd = 'sd' + genSd() + genSdNo()
            store.setItem('device', sd)
        } else
            sd = store.getItem('device')
        modifyHTML('<div>:: performing fsck on \'/dev/' + sd + '\'</div>', msg8, [500, 300])
    }
    let msg8 = () => modifyHTML('<div>:: mounting \'/dev/' + sd + '\' on real root</div>', msg9, [175, 120])
    let msg9 = () => modifyHTML('<div>:: running late hook [lvm2]</div>', msg10, [145, 95])
    let msg10 = () => modifyHTML('<div>:: running cleanup hook [lvm2]</div>', msg11, [110, 75])
    let msg11 = () => modifyHTML('<div>:: running cleanup hook [udev]</div>', msg12, [125, 100])
    let msg12 = () => modifyHTML('<br><div>Welcome to <brightblue>Hack OS</brightblue></div><br>', msg13, [375, 300])
    let msg13 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target Remote File Systems.</div>', msg14, [25, 25])
    let msg14 = () => modifyHTML('<div>[  <green>OK</green>  ] Listening on LVM2 poll daemon socket.</div>', msg15, [10, 5])
    let msg15 = () => modifyHTML('<div>[  <green>OK</green>  ] Listening on udev Control Socket.</div>', msg16, [10, 5])
    let msg16 = () => modifyHTML('<div>[  <green>OK</green>  ] Set up automount Arbitrary Executable File Formats File System Automount Point.</div>', msg17, [10, 5])
    let msg17 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Dispatch Password Requests to Console Directory Watch.</div>', msg18, [10, 5])
    let msg18 = () => modifyHTML('<div>[  <green>OK</green>  ] Listening on initctl Compatibility Named Pipe.</div>', msg19, [10, 5])
    let msg19 = () => modifyHTML('<div>[  <green>OK</green>  ] Created slice User and Session Slice.</div>', msg20, [10, 5])
    let msg20 = () => modifyHTML('<div>[  <green>OK</green>  ] Created slice system-systemd\\x2dfsck.slice.</div>', msg21, [10, 5])
    let msg21 = () => modifyHTML('<div>[  <green>OK</green>  ] Listening on Journal Socket.</div>', msg22, [100, 75])
    let msg22 = () => modifyHTML('<div>[      ] Starting Set Up Additional Binary Formats...</div>', msg23, [10, 5])
    let msg23 = () => modifyHTML('<div>[      ] Mounting POSIX Message Queue File System...</div>', msg24, [10, 5])
    let msg24 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target Login Prompts.</div>', msg25, [10, 5])
    let msg25 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target Slices.</div>', msg26, [10, 5])
    let msg26 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Forward Password Requests to Wall Directory Watch.</div>', msg27, [10, 5])
    let msg27 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target Paths.</div>', msg28, [10, 5])
    let msg28 = () => modifyHTML('<div>[      ] Starting Create list of required static devices for nodes for the current kernel...</div>', msg29, [10, 5])
    let msg29 = () => modifyHTML('<div>[  <green>OK</green>  ] Created slice system-getty.slice.</div>', msg30, [10, 5])
    let msg30 = () => modifyHTML('<div>[  <green>OK</green>  ]  Reached target Local Encrypted Volumes.</div>', msg31, [10, 5])
    let msg31 = () => modifyHTML('<div>[  <green>OK</green>  ] Listening on Journal Audit Socket.</div>', msg32, [10, 5])
    let msg32 = () => modifyHTML('<div>[      ] Mounting Huge Pages File System...</div>', msg33, [10, 5])
    let msg33 = () => modifyHTML('<div>[      ] Starting Remount Root and Kernel File Systems...</div>', msg34, [10, 5])
    let msg34 = () => modifyHTML('<div>[      ] Mounting Kernel Debug File System...</div>', msg35, [10, 5])
    let msg35 = () => modifyHTML('<div>[  <green>OK</green>  ] Listening on Process Core Dump Socket.</div>', msg36, [10, 5])
    let msg36 = () => modifyHTML('<div>[  <green>OK</green>  ] Listening on LVM2 metadata daemon socket.</div>', msg37, [10, 5])
    let msg37 = () => modifyHTML('<div>[  <green>OK</green>  ] Listening on udev Kernel Socket.</div>', msg38, [10, 5])
    let msg38 = () => modifyHTML('<div>[  <green>OK</green>  ] Listening on Journal Socket (/dev/log).</div>', msg39, [10, 5])
    let msg39 = () => modifyHTML('<div>[      ] Starting Load Kernel Modules...</div>', msg40, [10, 5])
    let msg40 = () => modifyHTML('<div>[  <green>OK</green>  ] Listening on Device-mapper event daemon daemon FIFOs.</div>', msg41, [10, 5])
    let msg41 = () => modifyHTML('<div>[      ] Starting Monitoring of LVM2 mirrors, snapshots etc. using dmeventd or progress polling...</div>', msg42, [10, 5])
    let msg42 = () => modifyHTML('<div>[      ] Starting Journal Service...</div>', msg43, [10, 5])
    let msg43 = () => modifyHTML('<div>[      ] Starting udev Coldplug all Devices...</div>', msg44, [10, 5])
    let msg44 = () => modifyHTML('<div>[      ] Mounting Arbitrary Executable File Formats File System...</div>', msg45, [10, 5])
    let msg45 = () => modifyHTML('<div>[  <green>OK</green>  ] Mounted POSIX Message Queue File System.</div>', msg46, [10, 5])
    let msg46 = () => modifyHTML('<div>[  <green>OK</green>  ] Mounted Huge Pages File System.</div>', msg47, [10, 5])
    let msg47 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Remount Root and Kernel File Systems.</div>', msg48, [10, 5])
    let msg48 = () => modifyHTML('<div>[  <green>OK</green>  ] Mounted Kernel Debug File System.</div>', msg49, [10, 5])
    let msg49 = () => modifyHTML('<div>[  <green>OK</green>  ] Mounted Arbitrary Executable File Formats File System.</div>', msg50, [10, 5])
    let msg50 = () => modifyHTML('<div>[      ] Starting Load/Save Random Seed...</div>', msg51, [10, 5])
    let msg51 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Journal Service.</div>', msg52, [10, 5])
    let msg52 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Set Up Additional Binary Formats.</div>', msg53, [10, 5])
    let msg53 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Create list of required static device nodes for the current kernel.</div>', msg54, [10, 5])
    let msg54 = () => modifyHTML('<div>[      ] Starting Create Static Device Modes in /dev...</div>', msg55, [10, 5])
    let msg55 = () => modifyHTML('<div>[      ] Starting Flush Journal to Persistent Storage...</div>', msg56, [10, 5])
    let msg56 = () => modifyHTML('<div>[  <green>OK</green>  ] Started udev Coldplug all Devices.</div>', msg57, [10, 5])
    let msg57 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Load/Save Random Seed.</div>', msg58, [10, 5])
    let msg58 = () => modifyHTML('<div>[  <green>OK</green>  ] Started LVM2 metadata daemon.</div>', msg59, [10, 5])
    let msg59 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Create Static Devices Nodes in /dev.</div>', msg60, [10, 5])
    let msg60 = () => modifyHTML('<div>[      ] Starting udev Kernel Device Manager...</div>', msg61, [10, 5])
    let msg61 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Monitoring of LVM2 mirrors, snapshots etc. using dmeventd or pr</div>', msg62, [10, 5])
    let msg62 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target Local File Systems (Pre).</div>', msg63, [10, 5])
    let msg63 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Load Kernel Modules.</div>', msg64, [10, 5])
    let msg64 = () => modifyHTML('<div>[      ] Starting Apply Kernel Variables...</div>', msg65, [10, 5])
    let msg65 = () => modifyHTML('<div>[      ] Mounting Kernel Configuration File System...</div>', msg66, [10, 5])
    let msg66 = () => modifyHTML('<div>[  <green>OK</green>  ] Mounted Kernel Configuration File System.</div>', msg67, [10, 5])
    let msg67 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Apply Kernel Variables.</div>', msg68, [10, 5])
    let msg68 = () => modifyHTML('<div>[  <green>OK</green>  ] Started udev Kernel Device Manager.</div>', msg69, [10, 5])
    let msg69 = () => modifyHTML('<div>[  <green>OK</green>  ] Found device ST500LT012-1BG142 Linux\\x20swap.</div>', msg70, [10, 5])
    let msg70 = () => modifyHTML('<div>[      ] Activating swap /dev/disk/by-uuid/bec0u751-cb16-4c3a-92aa-3aee099ab012...</div>', msg71, [10, 5])
    let msg71 = () => modifyHTML('<div>[<red>FAILED</red>] Failed to listen on Load/Save RF Kill Switch Status /dev/rfkill Watch.<br>See `systemctl status systemd-rfkill.socket` for details.</div>', msg72, [500, 425])
    let msg72 = () => modifyHTML('<div>[  <green>OK</green>  ] Activated swap /dev/disk/by-uuid/bec0u751-cb16-4c3a-92aa-3aee099ab012.</div>', msg73, [10, 5])
    let msg73 = () => modifyHTML('<div>[  <green>OK</green>  ] Created slice system-ystemd\\x2dbacklight.slice.</div>', msg74, [10, 5])
    let msg74 = () => modifyHTML('<div>[      ] Starting Load/Save Screen Backlight Brightness of backlight:intel_backlight...</div>', msg75, [10, 5])
    let msg75 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target Swap.</div>', msg76, [10, 5])
    let msg76 = () => modifyHTML('<div>[      ] Mounting Temporary Directory (/tmp)...</div>', msg77, [10, 5])
    let msg77 = () => modifyHTML('<div>[  <green>OK</green>  ] Mounted Temporary Directory (/tmp).</div>', msg78, [10, 5])
    let msg78 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Load/Save Screen Backlight Brightness of backlight:intel_backlight.</div>', msg79, [10, 5])
    let msg79 = () => modifyHTML('<div>[  <green>OK</green>  ] Found device ST500LT012-1BG142 SYSTEM_DRV.</div>', msg80, [10, 5])
    let msg80 = () => modifyHTML('<div>[      ] Starting File System Check on /dev/disk/by-uuid/6C86-FFF7...</div>', msg81, [600, 300])
    let msg81 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Flush Journal to Persistent Storage.</div>', msg82, [10, 5])
    let msg82 = () => modifyHTML('<div>[  <green>OK</green>  ] Started File System Check on /dev/disk/by-uuid/6C86-FFF7.</div>', msg83, [10, 5])
    let msg83 = () => modifyHTML('<div>[      ] Mounting /boot...</div>', msg84, [10, 5])
    let msg84 = () => modifyHTML('<div>[  <green>OK</green>  ] Mounted /boot.</div>', msg85, [10, 5])
    let msg85 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target Local File System.</div>', msg86, [10, 5])
    let msg86 = () => modifyHTML('<div>[      ] Starting Create Volatile Files and Directories...</div>', msg87, [10, 5])
    let msg87 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Create Volatile Files and Directories.</div>', msg88, [10, 5])
    let msg88 = () => modifyHTML('<div>[      ] Starting Update UTMP about System Boot/Shutdown...</div>', msg89, [10, 5])
    let msg89 = () => modifyHTML('<div>[      ] Starting Network Time Synchronization...</div>', msg90, [10, 5])
    let msg90 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Update UTMP about System Boot/Shutdown.</div>', msg91, [10, 5])
    let msg91 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Network Time Synchronization.</div>', msg92, [10, 5])
    let msg92 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target System Time Synchronized.</div>', msg93, [10, 5])
    let msg93 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target System initialization.</div>', msg94, [10, 5])
    let msg94 = () => modifyHTML('<div>[  <green>OK</green>  ] Listening on D-Bus System Message Bus Socket.</div>', msg95, [10, 5])
    let msg95 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target Sockets.</div>', msg96, [10, 5])
    let msg96 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target Basic System.</div>', msg97, [10, 5])
    let msg97 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Daily verification of password and group files.</div>', msg98, [10, 5])
    let msg98 = () => modifyHTML('<div>[      ] Starting dhcpcd on all interfaces...</div>', msg99, [10, 5])
    let msg99 = () => modifyHTML('<div>[      ] Starting Bluetooth service...</div>', msg100, [10, 5])
    let msg100 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Daily man-db cache update.</div>', msg101, [10, 5])
    let msg101 = () => modifyHTML('<div>[      ] Starting Login Service...</div>', msg102, [10, 5])
    let msg102 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target Sound Card.</div>', msg103, [10, 5])
    let msg103 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Daily Cleanup of Temporary Directories.</div>', msg104, [10, 5])
    let msg104 = () => modifyHTML('<div>[      ] Starting Apply cpupower configuration...</div>', msg105, [10, 5])
    let msg105 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Daily rotation of log files.</div>', msg106, [10, 5])
    let msg106 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target Timers.</div>', msg107, [10, 5])
    let msg107 = () => modifyHTML('<div>[  <green>OK</green>  ] Started D-Bus System Message Bus.</div>', msg108, [10, 5])
    let msg108 = () => modifyHTML('<div>[      ] Starting Network Manager...</div>', msg109, [10, 5])
    let msg109 = () => modifyHTML('<div>[  <green>OK</green>  ] Started dhcpcd on all interfaces.</div>', msg110, [10, 5])
    let msg110 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Apply cpupower configuration.</div>', msg111, [10, 5])
    let msg111 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Bluetooth service.</div>', msg112, [10, 5])
    let msg112 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target Bluetooth.</div>', msg113, [10, 5])
    let msg113 = () => modifyHTML('<div>[      ] Starting Hostname service...</div>', msg114, [10, 5])
    let msg114 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Hostname Service.</div>', msg115, [10, 5])
    let msg115 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Network Manager.</div>', msg116, [10, 5])
    let msg116 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target Network.</div>', msg117, [10, 5])
    let msg117 = () => modifyHTML('<div>[      ] Starting Permit User Sessions...</div>', msg118, [10, 5])
    let msg118 = () => modifyHTML('<div>[  <green>OK</green>  ] Started permit User Sessions.</div>', msg119, [10, 5])
    let msg119 = () => modifyHTML('<div>[      ] Starting Light Display Manager...</div>', msg120, [10, 5])
    let msg120 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target Multi-User System.</div>', msg121, [10, 5])
    let msg121 = () => modifyHTML('<div>[      ] Starting TLP system startup/shutdown...</div>', msg122, [10, 5])
    let msg122 = () => modifyHTML('<div>[      ] Starting Network Manager Script Dispatcher Service...</div>', msg123, [10, 5])
    let msg123 = () => modifyHTML('<div>[  <green>OK</green>  ] Started TLP system startup/shutdown.</div>', msg124, [10, 5])
    let msg124 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Light Display Manager.</div>', msg125, [10, 5])
    let msg125 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target Graphical Interface.</div>', msg126, [10, 5])
    let msg126 = () => modifyHTML('<div>[  <green>OK</green>  ] Started Network Manager Script Dispatcher Service.</div>', msg127, [10, 5])
    let msg127 = () => modifyHTML('<div>[  <green>OK</green>  ] Reached target User and Group Name Lookup.</div>', msg128, [10, 5])
    let msg128 = () => modifyHTML('<div>[      ] Starting accounts Service...</div>', msg129, [10, 5])
    let msg129 = () => modifyHTML('<div>[      ] Starting Authorization Manager...</div>', msg130, [10, 5])
    let msg130 = () => modifyHTML('<div>[      ] Starting WPA supplicant...</div>', msg131, [10, 5])
    let msg131 = () => modifyHTML('<div>[  <green>OK</green>  ] Started WPA supplicant.</div>', toLogin, [1000, 750])
    let toLogin = () => {
        setTimeout(() => {
            localStorage.setItem("currently_logged_in", null)
            $('#taskbar').css('display', 'block')
            $('#taskbar > *').css('display', 'inline-block')
            $('html, body').css('cursor', 'initial')
            $('#login').css('display', 'block')
            document.getElementById('bootloader').style.opacity = 0
            setTimeout(() => {
                document.getElementById('bootloader').style.display = 'none'
            }, 1500)
        }, Math.random()*1000+1000)
    }

    window.mobileAndTabletcheck = function() {
        var check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
        return check;
    };

    if (!mobileAndTabletcheck())
        launch()
    else {
        $('#taskbar, #taskbar > *, #bootloader').css('display', 'none')
        $('.start_unavailable').css('display', 'block')
    }
}

bootloader()