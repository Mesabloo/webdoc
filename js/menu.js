$('#taskbar #menu').click(() => {
    $('#taskbar_menu').css('top', '0px')
})

$('#taskbar #menu, #taskbar_menu').hover((e) => {
    if (e.type === 'mouseenter')
        $('#taskbar_menu').css('top', '0px')
    else if (e.type === 'mouseleave')
        $('#taskbar_menu').css('top', '-100%')
    else
        $('#taskbar_menu').css('top', '0px')
})

$('#taskbar_menu #logout').click(() => {
    if (localStorage.getItem('currently_logged_in') !== 'null') {
        console.log(`'${localStorage.getItem('currently_logged_in')}'`)
        background.pause()
        background.volume = 0
        // disconnect
        hideTerminal()
        hideImageViewer()
        hideVideoPlayer()
        hideEmailClient()
        $('#desktop').css('display', 'none')
        $('#login').css({
            'animation-name': 'fadeInUp',
            'animation-duration': '0.5s',
            'animation-fill-mode': 'forwards'
        })
        localStorage.setItem('currently_logged_in', null)
    }
})

$('#taskbar_menu #poweroff').click(() => {
    // show bootunloader
    setTimeout(() => {
        $('#bootunloader').css('display', 'inline-block')
        setTimeout(() => {
            $('#bootunloader').css('opacity', 1)
            setTimeout(() => {
                bootunloader()
            }, 1000)
        }, 100)
    }, Math.random()*1000+750)
    background.pause()
    background.volume = 0
})

$('#taskbar_menu #suspend').click(() => {
    // hide everything
    $('#suspender').css('display', 'inline-block')
    $('#suspender #unsuspend').css('display', 'inline-block')
    background.pause()
    background.volume = 0
})

$('#suspender #unsuspend').click(() => {
    $('#suspender').css('display', 'none')
    $('#suspender #unsuspend').css('display', 'none')
    background.play()
    background.volume = 0.5
})